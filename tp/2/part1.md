# Partie 1 : SSH

- [Partie 1 : SSH](#partie-1--ssh)
- [I. Intro](#i-intro)
- [II. Setup du serveur SSH](#ii-setup-du-serveur-ssh)
  - [1. Installation du serveur](#1-installation-du-serveur)
  - [2. Lancement du service SSH](#2-lancement-du-service-ssh)
  - [3. Etude du service SSH](#3-etude-du-service-ssh)
  - [4. Modification de la configuration du serveur](#4-modification-de-la-configuration-du-serveur)

# I. Intro

> *SSH* c'est pour *Secure SHell*.

***SSH* est un outil qui permet d'accéder au terminal d'une machine, à distance, en connaissant l'adresse IP de cette machine.**

*SSH* repose un principe de *client/serveur* :

- le *serveur*
  - est installé sur une machine par l'admin
  - écoute sur le port 22/TCP par convention
- le *client*
  - connaît l'IP du serveur
  - se connecte au serveur à l'aide d'un programme appelé "*client* *SSH*"

Pour nous, ça va être l'outil parfait pour contrôler toutes nos machines virtuelles.

Dans la vie réelle, *SSH* est systématiquement utilisé pour contrôler des machines à distance. C'est vraiment un outil de routine pour tout informaticien, qu'on utilise au quotidien sans y réfléchir.

> *Si vous louez un serveur en ligne, on vous donnera un accès SSH pour le manipuler la plupart du temps.*

![Feels like a hacker](./pics/feels_like_a_hacker.jpg)

# II. Setup du serveur SSH

Toujours la même routine :

- **1. installation de paquet**
  - avec le gestionnaire de paquet de l'OS
- **2. configuration** dans un fichier de configuration
  - avec un éditeur de texte
  - les fichiers de conf sont de simples fichiers texte
- **3. lancement du service**
  - avec une commande `systemctl start <NOM_SERVICE>`

> ***Pour toutes les commandes tapées qui figurent dans le rendu, je veux la commande ET son résultat. S'il manque l'un des deux, c'est useless.***

## 1. Installation du serveur

Sur les OS GNU/Linux, les installations se font à l'aide d'un gestionnaire de paquets.

🌞 **Installer le paquet `openssh-server`**

- avec une commande `apt install`

---

Note : une fois que le paquet est installé, plusieurs nouvelles choses sont dispos sur la machine. Notamment :

- un service `ssh`
- un dossier de configuration `/etc/ssh/`

## 2. Lancement du service SSH

🌞 **Lancer le service `ssh`**

- avec une commande `systemctl start`
- vérifier que le service est actuellement actif avec une commande `systemctl status`

> Vous pouvez aussi faire en sorte que le *service* SSH se lance automatiquement au démarrage avec la commande `systemctl enable ssh`.

## 3. Etude du service SSH

🌞 **Analyser le service en cours de fonctionnement**

- afficher le statut du *service*
  - avec une commande `systemctl status`
- afficher le/les processus liés au *service* `ssh`
  - avec une commande `ps`
  - isolez uniquement la/les ligne(s) intéressante(s) pour le rendu de TP
- afficher le port utilisé par le *service* `ssh`
  - avec une commande `ss -l`
  - isolez uniquement la/les ligne(s) intéressante(s)
- afficher les logs du *service* `ssh`
  - avec une commande `journalctl`
  - en consultant un dossier dans `/var/log/`
  - ne me donnez pas toutes les lignes de logs, je veux simplement que vous appreniez à consulter les logs

---

🌞 **Connectez vous au serveur**

- depuis votre PC, en utilisant un **client SSH**

> *La commande `ssh` de votre terminal, c'est un client SSH.*

## 4. Modification de la configuration du serveur

Pour modifier comment un *service* se comporte il faut modifier le fichier de configuration. On peut tout changer à notre guise.

🌞 **Modifier le comportement du service**

- c'est dans le fichier `/etc/ssh/sshd_config`
  - c'est un simple fichier texte
  - modifiez-le comme vous voulez, je vous conseille d'utiliser `nano` en ligne de commande
- effectuez le modifications suivante :
  - changer le ***port d'écoute*** du service *SSH*
    - peu importe lequel, il doit être compris entre 1025 et 65536
  - vous me prouverez avec un `cat` que vous avez bien modifié cette ligne
- pour cette modification, prouver à l'aide d'une commande qu'elle a bien pris effet
  - une commande `ss -l` pour vérifier le port d'écoute

> Vous devez redémarrer le service avec une commande `systemctl restart` pour que les changements inscrits dans le fichier de configuration prennent effet.

🌞 **Connectez vous sur le nouveau port choisi**

- depuis votre PC, avec un *client SSH*

![When she tells you](./pics/when_she_tells_you.png)
