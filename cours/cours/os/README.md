# OS

Ce cours a pour but de décrire les principaux constituants d'un OS, et donc par ce biais, les principales fonctions de l'OS.

Aufait aufait, *OS* c'est pour *Operating System* ou *Système d'exploitation* en français.

> Afin de faciliter la lecture de ce doc, on rappelle que les termes suivants sont des synonymes : "application", "logiciel", "soft", "software", "programme".

![Linux COVID](./pics/linux_covid.jpg)

# Sommaire

- [OS](#os)
- [Sommaire](#sommaire)
- [I. Rappels sur l'OS](#i-rappels-sur-los)
- [II. Principaux constituants de l'OS](#ii-principaux-constituants-de-los)
  - [1. Overview](#1-overview)
  - [2. Gestion de fichiers](#2-gestion-de-fichiers)
  - [3. Gestion d'utilisateurs](#3-gestion-dutilisateurs)
  - [4. Gestion de processus](#4-gestion-de-processus)
  - [5. Gestion de périphériques](#5-gestion-de-périphériques)
  - [6. Gestion du réseau](#6-gestion-du-réseau)
  - [7. Gestion de l'heure](#7-gestion-de-lheure)
  - [8. Shells](#8-shells)

# I. Rappels sur l'OS

➜ **L'OS est le programme le plus important sur toute machine, avec le noyau.** Les deux sont étroitement liés. La plupart du temps, quand vous installez un OS, il est livré avec son noyau.

> Voir [le cours d'intro à Linux](../cours/intro/README.md) pour plus de détails sur le noyau.

**En vérité, l'OS est constitué de plusieurs programmes**, chacun remplissant une fonction bien spécifique.

➜ **Toutes ces fonctions rassemblées constituent toutes les fonctions que l'on attend d'un OS.**

Le but de ce document est d'aborder plusieurs fonctions attendues d'un OS. Ceci pour se représenter un peu mieux ce qu'est concrètement OS, et de démystifier le terme.

# II. Principaux constituants de l'OS

## 1. Overview

L'OS, comme dit plus haut, est un ensemble de programme.

**L'OS lui-même est le premier programme qui est lancé lorsqu'on démarre une machine.**

**C'est lui qui a la tâche de lancer tous les autres programmes nécessaires au bon fonctionnement du système**, et de permettre à l'utilisateur d'utiliser le système.

## 2. Gestion de fichiers

L'OS va gérer les fichiers de la machine.

Cela implique :

- gérer les différentes partitions des disques durs
- proposer une arborescente de fichiers cohérente
- permettre à l'utilisateur d'interagir avec les fichiers

---

➜ Pour **la gestion de partition**, on verra ça dans un cours dédié

---

➜ **Une arborescence de fichiers**

En effet, les fichiers vous sont accessibles sous forme d'arbre.  

**Un dossier "racine" contient d'autres dossiers et fichiers.** Chaque dossier peut à sont tour contenir des sous-dossiers et fichiers. Et ainsi de suite.

**Dans Windows, il existe plusieurs racines** (ou *root* en anglais) : une pour chaque périphérique de stockage qu'on branche à la machine. On parle ici des dossiers `C:/`, `D:/`, etc. avec lesquels sont familiers les utilisateurs de Windows.

**Dans les OS GNU/Linux ou MacOS, il n'existe qu'une seule racine** : c'est le dossier `/`. Vu qu'il n'existe qu'une seule racine, on désigne ce dossier sobrement par le terme "la racine".

---

➜ **Permettre à l'utilisateur d'interagir avec les fichiers**

Ceci se fait par la fourniture d'une application qui est capable de comprendre la structure arborescente mise en place par l'OS.  

**Cette structure arborescente mise en place par l'OS est appelée *système de fichiers* ou *filesystem*.**

En GUI (interface graphique) on utilise un *explorateur de fichiers* pour explorer le *filesystem*.

En CLI (ligne de commande), les *shells* permettent d'interagir avec le *filesystem*.  

> Ce sont, par exemple, les commandes `mkdir`, `cat`, `ls`, etc. quand on se trouve dans un *shell* `bash` au sein d'un OS GNU/Linux.

C'est grâce à ces outils (explorateur de fichier ou *shell*) que l'utilisateur peut lire ou écrire dans des fichiers et dossiers.  
**C'est aussi comme ça que l'utilisateur va exécuter des programmes** (depuis un explorateur de fichier ou un *shell*).

> Bah ui, lancer un `.exe` depuis un *shell* ou en double-cliquant dessus, c'est pareil !

## 3. Gestion d'utilisateurs

L'OS gère aussi plusieurs trucs en ce qui concerne les utilisateurs :

- il entretient la liste des utilisateurs et leur mot de passe respectif
- il gère les permissions sur les fichiers
  - et ui c'est lié à la gestion d'utilisateurs ça
  - en effet, chaque fichier a un propriétaire : il appartient à un utilisateur
- il permet aux utilisateurs de se connecter sur la machine en échange de leur mot de passe

---

➜ **Liste des utilisateurs et mot de passe**

Sur GNU/Linux, la liste des utilisateurs est enregistrée dans le fichier `/etc/passwd`.  
La liste des mots de passe est dans `/etc/shadow`.

---

➜ **Gestion des permissions**

Il existe [un cours-notion dédié au sujet des permissions](../notions/permissions/README.md).

## 4. Gestion de processus

➜ **L'OS (et le noyau) va gérer la liste des processus en cours d'exécution.**

Il va déterminer à quel processus il faut accorder de l'attention, et ceux qui en ont moins besoin.

Il va aussi attribuer un identifiant unique à chaque processus.

On peut consulter facilement la liste des processus en cours d'exécution avec le Gestionnaire des Tâches sous Windows, ou la commande `ps` sur les systèmes MacOS et GNU/Linux.  
Il existe évidemment une commande sous Windows, et de jolies interfaces sous GNU/Linux et Mac, je cite simplement les outils les plus utilisés.

> On utilise souvent `ps -ef` sous GNU/Linux. Et `ps aux` sur MacOS.

![u'd be my realtime priority](./pics/process_priority.jpg)

---

➜ **L'OS a aussi la tâche d'entretenir les *services*.**

Les *services* sont de simples programmes que lance l'OS. Simplement, bah c'est l'OS qui les lance, pas un utilisateur.  
Plusieurs *services* sont essentiels au bon fonctionnement de l'OS.

> Par exemple il existe un service qui va gérer le bluetooth si votre PC a une puce bluetooth. Un service pour gérer l'heure de la machine. Un service pour gérer votre interface graphique. Etc.

Il existe [un cours-notion dédié aux *serveurs* et *services*](../notions/serveur/README.md).

## 5. Gestion de périphériques

**L'OS gère les périphériques.** Un périphérique c'est un truc physique qu'on branche à la carte mère d'une machine.

N'importe quoi qu'on branche à une machine, c'est un périphérique. Que ce soit sur un port VGA, USB, PCI, etc. Peu importe. PERIPHERIQUE J'AI DIT.

L'OS, quand on branche un périphérique, le détecte, et a la lourde tâche de comprendre ce que c'est ti donc que le machin qu'on vient de brancher. Il va genre deviner. Strong.

Une fois qu'il a deviné quel périphérique c'était, il va discuter avec le périphérique et vous permettre de l'utiliser.

Brave garçon cet OS.

## 6. Gestion du réseau

L'OS gère la pile réseau du système. Ce qu'on appelle "pile réseau" c'est :

- gestion des carte réseau
- gestion du firewall
- quelques autres trucs hors-sujet pour nous

➜ **Gestion des cartes réseau**

L'OS va se charger de donner un nom au carte réseau. C'est aussi lui qui attribuera une IP à chaque carte réseau.

Si une IP doit être récupérée automatiquement avec un DHCP (comme votre box chez vous, qui vous file une IP automatiquement quand vous vous pointez sur le réseau), c'est l'OS qui va discuter avec le serveur DHCP en question, pour récupérer une IP automatiquement.

C'est aussi l'OS qui se chargera d'envoyer des paquets sur le réseau. Ou de distribuer au bon programme les paquets reçus (*"oh tiens, c'est une page HTML ça, c'est pour le navigateur Web Chrome"*, et hop le contenu du paquet est envoyé à Chrome, qui va lire l'HTML)

➜ **Gestion du firewall**

C'est encore l'OS qui gère ça : le filtrage des paquets.

Il existe [une section dédiée au firewall dans le cours-notion sur les ports](../notions/ports/../README.md#6-le-firewall).

## 7. Gestion de l'heure

C'est l'OS qui maintient le système à l'heure.

Ca paraît trivial, mais la gestion de l'heure est souvent critique au sein d'un système, et encore plus au sein d'une infrastructure composée d'une multitude de machines.

Le système peut se synchroniser sur des serveurs externes, afin d'être maintenu à l'heure en permanence (même quand il est rallumé après une longue période d'inactivité par exemple).

## 8. Shells

**L'OS va aussi proposer à l'utilisateur un ou plusieurs *shells*.**  

Un *shell* désigne tout programme qu'un humain peut comprendre, et qui lui permet d'exécuter des tâches dans l'OS.

> Par abus de langage, on désigne les interfaces en ligne de commande (CLI) par le terme *shell*. En vérité, les interfaces graphiques (GUI) sont aussi des *shells*.

Une fois qu'on a un shell, on peut exécuter des tâches. Par exemple :

- **lancer de nouveaux programmes**
  - c'est généralement pour ça qu'on utilise une machine
- visualiser des fichiers du système (super photo de chameow)
- modifier des fichiers du système
- créer un utilisateur
- etc.