# Notions

- [Permissions POSIX](./permissions.md)
- [Les termes "Serveur", "Client" et "Service"](./serveur/README.md)
- [La notion de "port" en réseau](./port/README.md)
- [L'encodage](./encodage/README.md)
- [Les *filesystems*](./filesystem/README.md)