# Filesystem

- [Filesystem](#filesystem)
- [I. Fonctionnement d'un disque dur](#i-fonctionnement-dun-disque-dur)
- [II. Filesystem](#ii-filesystem)
- [III. Le formatage des disques](#iii-le-formatage-des-disques)

Pour aborder le *filesystem* et comprendre à quoi il sert, il faut déjà saisir comment nos ordinateurs ont l'habitude de stocker et faire transiter des données.

On parle ici du stockage et du transit **d'un point de vue physique**.

# I. Fonctionnement d'un disque dur

> Ceci est évidemment un gros résumé, en se concentrant sur ce qui est essentiel au cours.

Un disque dur stocke les informations sous forme [*binaire*](../encodage/README.md#le-binaire).

**La raison à cela c'est que les disques durs mécaniques sont constitués d'aimants.** Tout p'tits, par millions.

**Chaque aimant correspond à un *bit* : c'est un 0 ou un 1.** La tête de lecture et d'écriture du disque dur est elle aussi constituée d'un aimant.

Ainsi, en passant au dessus du disque, la tête de lecture et d'écriture peut détecter la position d'un aimant (vers le haut = 1, vers le bas = 0), ou changer la position d'un aimant, en rapprochant la tête de l'aimant en question.

Bon c'est cool on peut stocker des données binaires. Mais nous dans la vie de tous les jours, on interagit pas trop avec les 0 et les 1 : on interagit avec des fichiers et des dossiers.

Pour ça, il faut, en plus de la capacité à stocker des données binaires, un ***filesystem***.

# II. Filesystem

**Un *filesystem* ou *système de fichiers* est un outil qui permet d'arranger les 0 et les 1 d'un périphérique de stockage (comme un disque dur) pour en faire une arborescence de fichiers et de dossiers.**

![YOU ARE A FIIILE](./pics/YOU_ARE_A_FILE.png)

COMMENT IL FAIT CA WESS ?! ET BIEN C'EST TRES SIMPLE JAMMY.

En partant du principe que le disque dur c'est une suite de bits, on peut numéroter chacun de ces bits.

Le *filesystem* est un tableau qui indique à quel bit commence tel fichier, et à quel bit il termine.

Ainsi, c'est le *filesystem* qui se charge de rendre le contenu du disque dur cohérent. C'est aussi le *filesystem* que l'on interroge avec un explorateur de fichiers ou un terminal.

**Donc, avec un explorateur de fichiers ou un terminal, on navigue dans l'arborescence de fichiers. C'est à dire qu'on navigue dans le *filesystem*.**

# III. Le formatage des disques

**L'opération de *formatage* (*formatting* en anglais) permet de créer un *filesystem* sur un périphérique de stockage.**

Les systèmes de fichiers les plus utilisés :

- sur Windows : NTFS
- sur GNU/Linux : ext4 (et btrfs)
- cross-platform : FAT (idéal pour les clés USB)
- mais y'en a plein plein d'autres..[.](https://github.com/svenstaro/memefs)

![I'm not FAT](./pics/im_not_fat_im_ntfs.jpg)